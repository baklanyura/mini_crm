<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\ClientController;
use App\Http\Controllers\Admin\CompanyController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false,'reset' => false,'confirm' => false,'verify' => false]);
Route::middleware(['auth'])->group(function () {
    Route::get('/',[AdminController::class ,'index'])->name('home');
    Route::name('admin.')->prefix('admin')->group(function (){
        Route::resource('/companies', CompanyController::class)->names('companies');
        Route::resource('/clients', ClientController::class)->names('clients');
    });
});

