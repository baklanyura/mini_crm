<?php

use App\Http\Controllers\Api\CrmApiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware(['api-token'])->name('api.')->group(function () {
    Route::name('crm.')->prefix('crm')->group(function (){
        Route::get('/companies',[CrmApiController::class,'companies'])->name('companies');
        Route::get('/clients/{company_id}',[CrmApiController::class,'clients'])->name('clients');
        Route::get('/client_companies/{client_id}',[CrmApiController::class,'client_companies'])->name('client_companies');
    });
});


