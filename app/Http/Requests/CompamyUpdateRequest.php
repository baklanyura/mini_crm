<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CompamyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'             => ['required','string',Rule::unique('companies')->ignore($this->id)],
            'email'            => ['required','email',Rule::unique('companies')->ignore($this->id)],
            'foundation_year'  => 'nullable|date',
            'description'      => 'required|string|max:250',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'foundation_year' => $this->foundation_year ? Carbon::create($this->foundation_year) : null
        ]);
    }
}
