<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ClientCompaniesResource;
use App\Http\Resources\ClientResource;
use App\Http\Resources\CompanyResource;
use App\Models\Client;
use App\Models\Company;
use App\Services\ClientService;
use App\Services\CompanyService;
use Symfony\Component\HttpFoundation\Response;

class CrmApiController extends Controller
{
    /**
     * Get all companies with clients
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function companies(CompanyService $companyService)
    {
        return CompanyResource::collection($companyService->getCompaniesWithClients());
    }

    /**
     * Get company clients  by company_id
     *
     * @param $company_id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Exception
     */
    public function clients($company_id, CompanyService $companyService)
    {
        return ClientResource::collection($companyService->getClientsWithPaginateByCompanyId($company_id));
    }

    /**
     * Get client companies by client_id
     *
     * @param $client_id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Exception
     */
    public function client_companies($client_id, ClientService $clientService)
    {
        return ClientCompaniesResource::collection($clientService->getCompaniesByClientId($client_id));
    }
}
