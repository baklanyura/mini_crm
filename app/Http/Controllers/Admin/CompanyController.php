<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompamyUpdateRequest;
use App\Http\Requests\CompanyStoreRequest;
use App\Models\Company;
use App\Services\CompanyService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(Request $request, CompanyService $companyService)
    {
        if ($request->ajax()) {
           return $companyService->getCompaniesDatatable();
        }
        return view('admin.pages.companies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('admin.pages.companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CompanyStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CompanyStoreRequest $request, CompanyService $companyService)
    {
        $companyService->create($request->validated());
        return redirect()->route('admin.companies.index')->with('success','Компанію успішно створено');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function show(Company $company)
    {
        return view('admin.pages.companies.show',compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Company $company
     * @return Application|Factory|View
     */
    public function edit(Company $company)
    {
        return view('admin.pages.companies.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CompamyUpdateRequest $request
     * @param Company $company
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CompamyUpdateRequest $request, Company $company)
    {
        $company->update($request->validated());
        return redirect()->route('admin.companies.index')->with('success','Компанію успішно оновлено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Company $company
     * @return RedirectResponse
     */
    public function destroy(Company $company)
    {
        $company->delete();
        return redirect()->route('admin.companies.index')->with('success','Компанію успішно видалено');
    }
}
