<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClientStoreRequest;
use App\Http\Requests\ClientUpdateRequest;
use App\Http\Requests\CompamyUpdateRequest;
use App\Models\Client;
use App\Models\Company;
use App\Services\ClientService;
use App\Services\CompanyService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request, ClientService $clientService)
    {
        if ($request->ajax()) {
            return $clientService->getClientsDatatable();
        }
        return view('admin.pages.clients.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(CompanyService $companyService)
    {
        $companies = $companyService->getCompaniesPluckIdName();
        return view('admin.pages.clients.create',compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ClientStoreRequest $request
     * @param ClientService $clientService
     * @return RedirectResponse
     */
    public function store(ClientStoreRequest $request, ClientService $clientService)
    {
        $clientService->create($request->validated());
        return redirect()->route('admin.clients.index')->with('success','Клієнта успішно створено');
    }

    /**
     * Display the specified resource.
     *
     * @param Client $client
     * @return Application|Factory|View
     */
    public function show(Client $client)
    {
        $client->load('companies:id,name');
        return view('admin.pages.clients.show',compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Client $client
     * @param CompanyService $companyService
     * @return Application|Factory|View
     */
    public function edit(Client $client, CompanyService $companyService)
    {
        $companies = $companyService->getCompaniesPluckIdName();
        $client->load('companies:id,name');
        return view('admin.pages.clients.edit',compact('client','companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ClientUpdateRequest $request
     * @param Client $client
     * @param ClientService $clientService
     * @return RedirectResponse
     */
    public function update(ClientUpdateRequest $request, Client $client, ClientService $clientService)
    {
        $clientService->update($client, $request->validated());
        return redirect()->route('admin.clients.index')->with('success','Дані клієнта успішно оновлено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Client $client
     * @return RedirectResponse
     */
    public function destroy(Client $client)
    {
        $client->delete();
        return redirect()->route('admin.clients.index')->with('success','Клієнта успішно видалено');
    }
}
