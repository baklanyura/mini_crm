<?php

namespace App\Services;

use App\Models\Client;
use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Illuminate\Support\HigherOrderCollectionProxy;
use Symfony\Component\HttpFoundation\Response;

class ClientService
{
    /**
     * Variable for current model
     *
     * @var Client
     */
    private $client;

    /**
     * Actions that are automatically performed when accessing this class
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Get companies by client_id
     *
     * @param $client_id
     * @return HigherOrderBuilderProxy|HigherOrderCollectionProxy|mixed
     * @throws \Exception
     */
    public function getCompaniesByClientId($client_id)
    {
        $client = $this->client->with('companies')->find($client_id);
        if (!$client) throw new \Exception('Client not found', Response::HTTP_NOT_FOUND);
        return $client->companies;
    }

    /**
     * Clients data datatable
     *
     * @return mixed
     * @throws \Exception
     */
    public function getClientsDatatable(){
        $template = 'admin.actions.template';
        $data = $this->client->query()
            ->select([
                'id',
                'name',
                'surname',
                'email',
            ])
            ->orderBy('created_at','desc')
            ->orderBy('name');
        return datatables()->of($data)
            ->addColumn('actions', '')
            ->editColumn('actions', function ($row) use ($template) {
                $routeKey = Client::ROUTE_KEY;
                return view($template, compact('row', 'routeKey'));
            })
            ->rawColumns(['actions'])
            ->toJson();
    }

    /**
     * Create data client with related companies
     *
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
            $client = $this->client->create($data);
            $client->companies()->attach($data['companies'] ?? null);
            return $client;
    }

    /**
     * Update data client with related companies
     *
     * @param $client
     * @param $data
     * @return mixed
     */
    public function update($client, $data)
    {
        $client->update($data);
        $client->companies()->sync($data['companies'] ?? null);
        return $client;
    }

}