<?php

namespace App\Services;

use App\Models\Company;
use Symfony\Component\HttpFoundation\Response;

class CompanyService
{
    /**
     * Variable for current model
     *
     * @var Company
     */
    private $company;

    /**
     * Actions that are automatically performed when accessing this class
     */
    public function __construct()
    {
        $this->company = new Company();
    }

    /**
     * Get companies with clients
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getCompaniesWithClients()
    {
        return $this->company->with('clients')
            ->withCount('clients')
            ->paginate();
    }

    /**
     * Get clients with paginate by company_id
     *
     * @param $company_id
     * @return mixed
     * @throws \Exception
     */
    public function getClientsWithPaginateByCompanyId($company_id)
    {
        $company = Company::with('clients')
            ->find($company_id);
        if (!$company) throw new \Exception('Company not found', Response::HTTP_NOT_FOUND);
        return $company->clients()->paginate();
    }

    /**
     * Get all companies pluck id name
     *
     * @return mixed
     */
    public function getCompaniesPluckIdName()
    {
        return $this->company->pluck('name','id');
    }

    /**
     * Companies data datatable
     *
     * @return mixed
     * @throws \Exception
     */
    public function getCompaniesDatatable()
    {
        $template = 'admin.actions.template';
        $data = $this->company->query()
            ->select([
                'id',
                'name',
                'email',
                'foundation_year',
                'description',
            ])
            ->orderBy('created_at','desc')
            ->orderBy('name');
        return datatables()->of($data)
            ->addColumn('actions', '')
            ->editColumn('actions', function ($row) use ($template) {
                $routeKey = Company::ROUTE_KEY;
                return view($template, compact('row', 'routeKey'));
            })
            ->rawColumns(['actions'])
            ->toJson();
    }

    /**
     * Create new  company
     *
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->company->create($data);
    }
}