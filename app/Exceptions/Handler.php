<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (\Exception $e, $request) {
            if ($request->is('api/*')) {
                if (method_exists($e,'getCode') && $e->getCode()) $code = $e->getCode();
                else if (method_exists($e,'getStatusCode') && $e->getStatusCode()) $code = $e->getStatusCode();
                else if (property_exists($e, 'status')) $code = $e->status;
                else $code = 500;
                return response()->json([
                    'message' => $e->getMessage(),
                ], $code);
            }
        });
    }
}
