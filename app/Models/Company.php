<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    const ROUTE_KEY = 'admin.companies';
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'foundation_year',
        'description',
    ];

    /**
     * Relationship with clients
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clients()
    {
        return $this->belongsToMany(Client::class);
    }

    /**
     * Set the companies foundation_year.
     *
     * @param $data
     * @return void
     */
    public function setFoundationYearAttribute($data)
    {
        $this->attributes['foundation_year'] = $data ? $data->format('Y') : null ;
    }
}
