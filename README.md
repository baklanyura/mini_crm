Запуск проэкта 
   1. Зробіть клонування проєкту 
    git clone https://gitlab.com/baklanyura/mini_crm.git
   2. Перейдіть в папку з проєктом  mini_crm
   3.Запустити докер
      docker compose  up -d
   4. Зайти в контейнер 
      docker exec -it crm-php bash
   5. Запустити команди всередині контейнера
         chown -R www-data:www-data storage/
         composer install
         composer run-script post-root-package-install
         composer run-script post-create-project-cmd
         php artisan migrate
         php artisan db:seed
         npm install && npm run dev
            
   6.Доступ до админпанелі
      http://127.0.0.1:8877
      login    :  admin@gmail.com
      password :  password
    
    База данных 
    crm@localhost: 8384
    User: dev
    Password: devpass
   7. Api роути:
    /--------------------------------------------------------------------------------------------
      Для тестування добавте в header 
      Authorization  : Bearer 0faf2838d0906db59dc5ba524fbcc1c47f4ac878e576a41f650f47b7b0f95bc1
   /---------------------------------------------------------------------------------------------
     GET  http://127.0.0.1:8877/api/crm/companies   (потрібна авторизація bearer token)
     (повинен повертати список компаній з клієнтами (clients) для кожної
     компанії та загальною кількістю клієнтів у форматі json з можливістю пагінації. У відповіді мають
     бути дані про компанію (id, name, email, foundation_year, clients, clients_count) та дані клієнтів (id,
     name, email, surname))
     Response
     {
         "data": [
             {
                 "id": 1,
                 "name": "dolor vero",
                 "email": "lynch.peter@zboncak.net",
                 "foundation_year": 1985,
                 "clients": [
                     {
                     "id": 2550,
                     "name": "delectus",
                     "email": "qfeest@gmail.com",
                     "surname": "aspernatur"
                     },
                     {
                     "id": 5406,
                     "name": "eius",
                     "email": "dorothea.zboncak@homenick.com",
                     "surname": "et"
                     },
                 ],
                 "clients_count": 10
             },
             ...
         ],
         "links": {
         "first": "http://127.0.0.1:8877/api/crm/companies?page=1",
         "last": "http://127.0.0.1:8877/api/crm/companies?page=734",
         "prev": null,
         "next": "http://127.0.0.1:8877/api/crm/companies?page=2"
         },
         "meta": {
         "current_page": 1,
         "from": 1,
         "last_page": 734,
         "links": [
         {
         "url": null,
         "label": "&laquo; Previous",
         "active": false
         },
         {
         "url": "http://127.0.0.1:8877/api/crm/companies?page=1",
         "label": "1",
         "active": true
         },
         {
         "url": "http://127.0.0.1:8877/api/crm/companies?page=2",
         "label": "2",
         "active": false
         },
         {
         "url": "http://127.0.0.1:8877/api/crm/companies?page=3",
         "label": "3",
         "active": false
         },
         {
         "url": "http://127.0.0.1:8877/api/crm/companies?page=4",
         "label": "4",
         "active": false
         },
         {
         "url": "http://127.0.0.1:8877/api/crm/companies?page=5",
         "label": "5",
         "active": false
         },
         {
         "url": "http://127.0.0.1:8877/api/crm/companies?page=6",
         "label": "6",
         "active": false
         },
         {
         "url": "http://127.0.0.1:8877/api/crm/companies?page=7",
         "label": "7",
         "active": false
         },
         {
         "url": "http://127.0.0.1:8877/api/crm/companies?page=8",
         "label": "8",
         "active": false
         },
         {
         "url": "http://127.0.0.1:8877/api/crm/companies?page=9",
         "label": "9",
         "active": false
         },
         {
         "url": "http://127.0.0.1:8877/api/crm/companies?page=10",
         "label": "10",
         "active": false
         },
         {
         "url": null,
         "label": "...",
         "active": false
         },
         {
         "url": "http://127.0.0.1:8877/api/crm/companies?page=733",
         "label": "733",
         "active": false
         },
         {
         "url": "http://127.0.0.1:8877/api/crm/companies?page=734",
         "label": "734",
         "active": false
         },
         {
         "url": "http://127.0.0.1:8877/api/crm/companies?page=2",
         "label": "Next &raquo;",
         "active": false
         }
         ],
         "path": "http://127.0.0.1:8877/api/crm/companies",
         "per_page": 15,
         "to": 15,
         "total": 11008
         }
       }
       /--------------------------------------------------------------------------------------------
       GET  http://127.0.0.1:8877/api/crm/clients/{company_id}  (потрібна авторизація bearer token)
       (приймає id компанії, повертає список клієнтів у json з можливістю
       пагінації. У відповіді мають бути дані про клієнта – id, name, email, surname;)
       Response
       {
             "data": [
                 {
                 "id": 285,
                 "name": "eum",
                 "email": "macy17@tremblay.com",
                 "surname": "repellat"
                 },
                 {
                 "id": 9029,
                 "name": "nesciunt",
                 "email": "chelsey59@emard.net",
                 "surname": "eligendi"
                 },
               ...
             ],
             "links": {
             "first": "http://127.0.0.1:8877/api/crm/clients/100?page=1",
             "last": "http://127.0.0.1:8877/api/crm/clients/100?page=1",
             "prev": null,
             "next": null
             },
             "meta": {
             "current_page": 1,
             "from": 1,
             "last_page": 1,
             "links": [
             {
             "url": null,
             "label": "&laquo; Previous",
             "active": false
             },
             {
             "url": "http://127.0.0.1:8877/api/crm/clients/100?page=1",
             "label": "1",
             "active": true
             },
             {
             "url": null,
             "label": "Next &raquo;",
             "active": false
             }
             ],
             "path": "http://127.0.0.1:8877/api/crm/clients/100",
             "per_page": 15,
             "to": 10,
             "total": 10
             }
       }
    /----------------------------------------------------------------------------------------
      GET  http://127.0.0.1:8877/api/crm/client_companies/{client_id} (потрібна авторизація bearer token)
      (приймає id клієнта, повертає список компаній, пов'язаних з
      клієнтом. У відповіді мають бути дані про компанію – id, name, emal, foundation_year;)
      Response
   {
      "data": [
          {
              "id": 7,
              "name": "nam earum",
              "email": "demarco.homenick@gmail.com",
              "foundation_year": 1976
          },
          {
              "id": 4847,
              "name": "occaecati voluptatem",
              "email": "jermain45@gmail.com",
              "foundation_year": 1998
          },
          ...
      ]
   }
