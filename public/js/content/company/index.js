jQuery(document).ready(function () {
    jQuery.noConflict();
    jQuery('#companies').DataTable({
        ajax: '/admin/companies',
        serverSide: true,
        processing: true,
        fixedHeader: true,
        columns: [
            {data: 'name', name: 'name', 'defaultContent': ''},
            {data: 'email', name: 'email', 'defaultContent': ''},
            {data: 'foundation_year', name: 'foundation_year', 'defaultContent': ''},
            {data: 'description', name: 'description', 'defaultContent': ''},
            {data: 'actions', name: 'actions', orderable: false, searchable: false}
        ]
    });
})
