jQuery(document).ready(function () {
    jQuery.noConflict();
    jQuery('#clients').DataTable({
        ajax: '/admin/clients',
        serverSide: true,
        processing: true,
        fixedHeader: true,
        columns: [
            {data: 'name', name: 'name', 'defaultContent': ''},
            {data: 'surname', name: 'surname', 'defaultContent': ''},
            {data: 'email', name: 'email', 'defaultContent': ''},
            {data: 'actions', name: 'actions', orderable: false, searchable: false}
        ]
    });
})
