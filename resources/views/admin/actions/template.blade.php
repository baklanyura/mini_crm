<div class="row">
    <div class="col-4">
        <a  href="{{ route($routeKey.'.show', $row) }}" ><i class="fas fa-eye text-info"></i></a>
    </div>
    <div class="col-4">
        <a href="{{ route($routeKey.'.edit', $row) }}" ><i class="fas fa-edit"></i></a>
    </div>
    <div class="col-4">
        <form action="{{ route($routeKey.'.destroy', $row) }}" method="post" id="destroyForm-{{ $row->id }}" style="display: none">
            @csrf
            @method('DELETE')
        </form>
        <a  href="" onclick="
        if(confirm('Ви дійсно хочете видалити даний єлемент')){
        event.preventDefault();
        document.getElementById('destroyForm-{{ $row->id }}').submit();
        }else{
        event.preventDefault();
        }">
            <i class="fas fa-trash text-danger"></i></a>
        </a>
    </div>

</div>



