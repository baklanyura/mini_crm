@extends('admin.layouts.app')
@section('content-page', 'Редагувати дані клієнта')
@section('content-title', 'Редагувати дані клієнта')
@section('content')

    <div class="container-fluid col-lg-8 mt-2">
        <form method="POST" action="{{ route('admin.clients.update', $client) }}">
            @csrf
            @method('PUT')
            <div class="form-group col-lg-12 mb-2">
                <label for="inputName" class="col-xs-2 control-label">Ім'я:</label>
                <div class="col-xs-8">
                    <input type="text" name="name" class="form-control" id="inputName" placeholder="Введіть ім'я"  value="{{old('name',$client->name)}}">
                    <input type="hidden" name="id" class="form-control"  value="{{ $client->id }}">
                    @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="form-group col-lg-12 mb-2">
                <label for="inputSurName" class="col-xs-2 control-label">Прізвище:</label>
                <div class="col-xs-8">
                    <input type="text" name="surname" class="form-control" id="inputSurName" placeholder="Введіть прізвище"  value="{{old('surname',$client->surname)}}">
                    @error('surname')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="form-group col-lg-12 mb-2">
                <label for="inputEmail" class="col-xs-2 control-label">Електрона адреса:</label>
                <div class="col-xs-8">
                    <input type="text" name="email" class="form-control" id="inputEmail" placeholder="Введіть електронну адресу" value="{{old('email',$client->email)}}">
                    @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="form-group col-lg-12 mb-2">
                <label for="inputCompanies" class="col-xs-2 control-label">Компанії клієнта:</label>
                <div class="col-xs-8">
                    <select name="companies[]" data-companies="{{ $client->companies->pluck('id') }}" class="form-control select2" multiple="multiple" id="inputCompanies"
                            data-placeholder="Виберіть компанію закріплену за клієнтом"
                    >
                        @foreach($companies as $id => $name)
                            <option value="{{ $id }}">{{ $name }}</option>
                        @endforeach
                    </select>
                    @error('companies')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group mt-5 col-lg-12 ">
                <div class="float-left ">
                    <a class="btn btn-outline-primary" href="{{ route('admin.clients.index') }}" title="Go back"> <i class="fas fa-backward "></i> </a>
                </div>
                <div class="col-xs-offset-2 col-xs-8 float-right">
                    <button type="submit" class="btn btn-outline-primary">Зберегти</button>
                </div>
            </div>
        </form>
    </div>

@endsection

@push('content-css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #3c8dbc;
            border-color: #367fa9;
            color: #fff;
        }
        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            margin-right: 5px;
            color: rgba(255,255,255,0.7);
            border-right: none;
        }
    </style>
@endpush

@push('content-js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
            let companies = $("#inputCompanies");
            companies.val(companies.data('companies')).trigger('change');
        });
    </script>
@endpush


