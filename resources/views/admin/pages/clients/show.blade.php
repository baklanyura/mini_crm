@extends('admin.layouts.app')
@section('content-page', 'Дані про клієнта')
@section('content-title')
    Дані про компанію {{ $client->name }}
@endsection

@section('content')
    <div class="container mt-2">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Ім'я:</strong>
                    {{ $client->name }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Прізвище:</strong>
                    {{ $client->surname }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Електрона адреса:</strong>
                    {{ $client->email }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong class="mr-3">Компанії клієнта:</strong>
                    @foreach($client->companies as $company)
                        <a class="mr-2"  href="{{ route('admin.companies.show', $company) }}" ><i class="fas fa-building text-info"></i>
                            {{$company->name}}
                        </a>
                    @endforeach
                </div>
            </div>

            <div class="col-xs-8 col-sm-8 col-md-8 mt-4">

                <div class="float-left mr-5">
                    <a class="btn btn-outline-primary" href="{{ route('admin.clients.index') }}" title="Go back"> <i class="fas fa-backward "></i> </a>
                </div>
                <div class="ml-5">
                    <a type="button" class="btn btn-outline-primary" href="{{ route('admin.clients.edit', $client) }}">Редагувати</a>
                </div>
            </div>
        </div>
    </div>
@endsection
