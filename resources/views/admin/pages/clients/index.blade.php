@extends('admin.layouts.app')

@section('content-page', 'Клієнти')
@section('content-title', 'Клієнти')
@section('content')
    <div class="container-fluid">
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="{{ route('admin.clients.create') }}" title="Create a client"> <i class="fas fa-plus-circle "></i>
                Додати клієнта</a>
        </div>
        <div class="card-alert m-3">
            <div class="card card-primary">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <table id="clients" class="table table-bordered table-condensed table-striped">
                        <thead>
                        <tr>
                            <th>Ім'я</th>
                            <th>Прізвище</th>
                            <th>Електрона адреса</th>
                            <th class="w-10"></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

@push('content-js')
    <script src="{{ asset('js/content/clients/index.js') }}" defer></script>
@endpush

@endsection

