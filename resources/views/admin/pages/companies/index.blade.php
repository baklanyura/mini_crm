@extends('admin.layouts.app')

@section('content-page', 'Компанії')
@section('content-title', 'Компанії')
@section('content')
    <div class="container-fluid">
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="{{ route('admin.companies.create') }}" title="Create a product"> <i class="fas fa-plus-circle "></i>
            Додати компанію</a>
        </div>

        <div class="card-alert m-3">
            <div class="card card-primary">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <table id="companies" class="table table-bordered table-condensed table-striped">
                        <thead>
                        <tr>
                            <th>Назва компанії</th>
                            <th>Електрона адреса компанії</th>
                            <th>Рік заснування компанії</th>
                            <th>Опис компанії</th>
                            <th class="w-10"></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

@push('content-js')
    <script src="{{ asset('js/content/company/index.js') }}" defer></script>
@endpush

@endsection

