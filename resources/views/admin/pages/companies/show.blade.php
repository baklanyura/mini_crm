@extends('admin.layouts.app')
@section('content-page', 'Дані про компанію')
@section('content-title')
    Дані про компанію {{ $company->name }}
@endsection

@section('content')
    <div class="container mt-2">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Назва компанії:</strong>
                    {{ $company->name }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Електрона адреса компанії:</strong>
                    {{ $company->email }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Рік заснування компанії:</strong>
                    {{ $company->foundation_year }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Опис компанії:</strong>
                    {{ $company->description }}
                </div>
            </div>

            <div class="col-xs-8 col-sm-8 col-md-8 mt-4">

                <div class="float-left mr-5">
                    <a class="btn btn-outline-primary" href="{{ route('admin.companies.index') }}" title="Go back"> <i class="fas fa-backward "></i> </a>
                </div>
                <div class="ml-5">
                    <a type="button" class="btn btn-outline-primary" href="{{ route('admin.companies.edit', $company->id) }}">Редагувати</a>
                </div>
            </div>
        </div>
    </div>
@endsection
