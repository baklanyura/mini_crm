@extends('admin.layouts.app')
@section('content-page', 'Додати компанію')
@section('content-title', 'Додати компанію')
@section('content')

<div class="container-fluid col-lg-8 mt-2">
    <form method="POST" action="{{ route('admin.companies.store') }}">
        @csrf
        <div class="form-group col-lg-12 mb-2">
            <label for="inputName" class="col-xs-2 control-label">Назва компанії:</label>
            <div class="col-xs-8">
                <input type="text" name="name" class="form-control" id="inputName" placeholder="Введіть назву компаніїї"  value="{{old('name')}}">
                @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="form-group col-lg-12 mb-2">
            <label for="inputEmail" class="col-xs-2 control-label">Електрона адреса компанії:</label>
            <div class="col-xs-8">
                <input type="text" name="email" class="form-control" id="inputEmail" placeholder="Введіть електронну адреса компанії" value="{{old('email')}}">
                @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="form-group col-lg-12 mb-2">
            <label for="inputFoundationYear" class="col-xs-2 control-label">Рік заснування компанії:</label>
            <div class="col-xs-8">
                <input type="number" name="foundation_year" min="1901" max="2099" step="1"  class="form-control" id="inputFoundationYear" placeholder="Введіть рік заснування компанії" value="{{old('foundation_year')}}">
                @error('foundation_year')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="form-group col-lg-12 mb-2">
            <label for="inputDescription" class="col-xs-2 control-label">Опис компанії:</label>
            <div class="col-xs-8">
                <textarea  name="description" maxlength="250" class="form-control" id="inputDescription" placeholder="Введіть опис компанії">{{ old('description') }}</textarea>
                @error('description')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="form-group mt-5 col-lg-12 ">
            <div class="float-left ">
                <a class="btn btn-outline-primary" href="{{ route('admin.companies.index') }}" title="Go back"> <i class="fas fa-backward "></i> </a>
            </div>
            <div class="col-xs-offset-2 col-xs-8 float-right">
                <button type="submit" class="btn btn-outline-primary">Створити</button>
            </div>
        </div>
    </form>
</div>

@endsection


