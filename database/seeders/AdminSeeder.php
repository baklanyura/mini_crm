<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      => 'Admin',
            'email'     => 'admin@gmail.com',
            'password'  =>  Hash::make('password'),
            'api_token' =>  '0faf2838d0906db59dc5ba524fbcc1c47f4ac878e576a41f650f47b7b0f95bc1'
        ]);
    }
}
