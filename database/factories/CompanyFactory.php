<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'            => $this->faker->unique()->words(2 , true),
            'email'           => $this->faker->unique()->email(),
            'foundation_year' => $this->faker->dateTime,
            'description'     => $this->faker->text(200),
        ];
    }
}
